package com.dev.potridersuser.activity.viewActivity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivityEditAccountBinding;

import spencerstudios.com.bungeelib.Bungee;

public class EditAccountActivity extends AppCompatActivity {


    ActivityEditAccountBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_account);




        initControls();
    }


    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(EditAccountActivity.this, ProfileActivity.class));
        finish();
        Bungee.swipeRight(EditAccountActivity.this);
    }
}
