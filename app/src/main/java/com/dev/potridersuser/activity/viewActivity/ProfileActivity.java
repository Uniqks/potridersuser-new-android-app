package com.dev.potridersuser.activity.viewActivity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivityProfileBinding;

import spencerstudios.com.bungeelib.Bungee;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {


    ActivityProfileBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);





        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.backIv.setOnClickListener(this);
        binding.editAccTv.setOnClickListener(this);
        binding.chngPwdTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIv:
                startActivity(new Intent(ProfileActivity.this, HomeActivity.class));
                finish();
                Bungee.swipeRight(ProfileActivity.this);
                break;

            case R.id.editAccTv:
                startActivity(new Intent(ProfileActivity.this, EditAccountActivity.class));
                finish();
                Bungee.swipeLeft(ProfileActivity.this);
                break;

            case R.id.chngPwdTv:
                startActivity(new Intent(ProfileActivity.this, ChangePwdActivity.class));
                finish();
                Bungee.swipeLeft(ProfileActivity.this);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProfileActivity.this, HomeActivity.class));
        finish();
        Bungee.swipeRight(ProfileActivity.this);
    }

}
