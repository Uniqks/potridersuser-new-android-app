package com.dev.potridersuser.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.potridersuser.R;
import com.dev.potridersuser.activity.viewActivity.ProfileActivity;
import com.dev.potridersuser.databinding.FragmentDrawerBinding;

import spencerstudios.com.bungeelib.Bungee;

public class DrawerFragment extends Fragment implements View.OnClickListener {


    FragmentDrawerBinding binding;
    private DrawerLayout mDrawerLayout;
    private View mContainerView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_drawer, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.profileLl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.profileLl:
                closeDrawer();
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().finish();
                Bungee.swipeLeft(getActivity());
                break;
        }
    }


    /**
     * Method is used to initialize drawer layout
     */
    public void initDrawerViews(int fragmentId, DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
        mContainerView = getActivity().findViewById(fragmentId);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mContainerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mContainerView);
    }

}
