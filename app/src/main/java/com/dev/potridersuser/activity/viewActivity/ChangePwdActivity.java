package com.dev.potridersuser.activity.viewActivity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivityChangePwdBinding;

import spencerstudios.com.bungeelib.Bungee;

public class ChangePwdActivity extends AppCompatActivity implements View.OnClickListener {


    ActivityChangePwdBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_pwd);





        initControls();
    }


    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        binding.backIv.setOnClickListener(this);
        binding.resetLl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIv:
                startActivity(new Intent(ChangePwdActivity.this, ProfileActivity.class));
                finish();
                Bungee.swipeRight(ChangePwdActivity.this);
                break;

            case R.id.resetLl:

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ChangePwdActivity.this, ProfileActivity.class));
        finish();
        Bungee.swipeRight(ChangePwdActivity.this);
    }
}
