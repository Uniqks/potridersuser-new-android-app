package com.dev.potridersuser.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivityForgotPwdBinding;

import spencerstudios.com.bungeelib.Bungee;

public class ForgotPwd extends AppCompatActivity implements View.OnClickListener {


    ActivityForgotPwdBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_pwd);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.backIv.setOnClickListener(this);
        binding.resetRl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIv:
                startActivity(new Intent(ForgotPwd.this, Login.class));
                finish();
                Bungee.swipeRight(ForgotPwd.this);
                break;

            case R.id.resetRl:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForgotPwd.this, Login.class));
        finish();
        Bungee.swipeRight(ForgotPwd.this);
    }
}
