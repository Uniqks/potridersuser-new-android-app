package com.dev.potridersuser.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.dev.potridersuser.R;
import com.dev.potridersuser.custom.CustBounceInterpolator;
import com.dev.potridersuser.databinding.ActivitySplashBinding;

import spencerstudios.com.bungeelib.Bungee;

public class Splash extends AppCompatActivity {


    ActivitySplashBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }



        initControls();
    }



    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        CustBounceInterpolator interpolator = new CustBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        binding.logoIv.startAnimation(myAnim);

        setHandler();
    }


    /**
     * set handler...
     */
    private void setHandler() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(Splash.this, Login.class));
                finish();
                Bungee.swipeLeft(Splash.this);
            }
        }, 1500);
    }
}
