package com.dev.potridersuser.activity.viewActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.potridersuser.R;
import com.dev.potridersuser.activity.fragment.DashboardFragment;
import com.dev.potridersuser.activity.fragment.DrawerFragment;
import com.dev.potridersuser.app.PotRiderApp;

import spencerstudios.com.bungeelib.Bungee;

public class HomeActivity extends BaseActivity implements View.OnClickListener {


    private Toolbar mToolbar;
    public ImageView mDrawerImg;
    public TextView mTitleTxt;
    private LinearLayout mainLl;
    private DrawerFragment mDrawerFragment;

    Fragment fragment;
    private PotRiderApp mPotRiderApp;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mPotRiderApp = (PotRiderApp) getApplication();



        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mDrawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.initDrawerViews(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        pushFragment(new DashboardFragment(), false, true, false, DashboardFragment.class.getSimpleName(), null, false);

        mDrawerImg = findViewById(R.id.drawerIv);
        mDrawerImg.setOnClickListener(this);

        mTitleTxt = findViewById(R.id.tvTitle);
        //mTitleTxt.setText("Home");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerIv:
                mPotRiderApp.hideKeyboard(HomeActivity.this);
                mDrawerFragment.openDrawer();
                break;

        }
    }




    public void replaceFragment(Fragment fragment) {

        this.fragment = fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            getSupportFragmentManager().popBackStack();
            System.out.println("on  back --->" + getSupportFragmentManager().getBackStackEntryCount());

            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                mTitleTxt.setText("Home");
            }

        } else {

            System.out.println("on  back1 --->" + getSupportFragmentManager().getBackStackEntryCount());
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;

            final View dialogView = View.inflate(HomeActivity.this, R.layout.dialog_signout,null);

            final Dialog dialog = new Dialog(HomeActivity.this, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);

            TextView mNoTxt, mYesTxt, mMessageTxt;

            mMessageTxt = dialog.findViewById(R.id.txt_msg);
            mMessageTxt.setText("Are you sure you want to exit?");

            mNoTxt = dialog.findViewById(R.id.txt_no);
            mYesTxt = dialog.findViewById(R.id.txt_yes);

            mNoTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            mYesTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();


            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);

        }
    }

}
