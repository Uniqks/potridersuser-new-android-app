package com.dev.potridersuser.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dev.potridersuser.R;
import com.dev.potridersuser.activity.viewActivity.HomeActivity;
import com.dev.potridersuser.app.PotRiderApp;
import com.dev.potridersuser.app.PotRiderSessionManager;
import com.dev.potridersuser.databinding.ActivityLoginBinding;
import com.dev.potridersuser.utils.CommonUtils;
import com.dev.potridersuser.utils.Validation;

import spencerstudios.com.bungeelib.Bungee;

public class Login extends AppCompatActivity implements View.OnClickListener {


    ActivityLoginBinding binding;
    private PotRiderApp mPotRiderApp;
    private PotRiderSessionManager mAppManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }

        mPotRiderApp = (PotRiderApp) getApplication();
        mAppManager = new PotRiderSessionManager(Login.this);


        if (mAppManager.isUserLoggedIn()) {
            startActivity(new Intent(Login.this, HomeActivity.class));
            finish();
            Bungee.slideLeft(Login.this);
        }

        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.loginRl.setOnClickListener(this);
        binding.fbLoginLl.setOnClickListener(this);
        binding.gplusLoginLl.setOnClickListener(this);
        binding.fpwdTv.setOnClickListener(this);
        binding.signupLl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.loginRl:
                mPotRiderApp.hideKeyboard(Login.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.loginRl));

                checkValidation();
                break;

            case R.id.fbLoginLl:
                break;

            case R.id.gplusLoginLl:
                break;

            case R.id.fpwdTv:
                startActivity(new Intent(Login.this, ForgotPwd.class));
                finish();
                Bungee.swipeLeft(Login.this);
                break;

            case R.id.signupLl:
                startActivity(new Intent(Login.this, Signup.class));
                finish();
                Bungee.swipeLeft(Login.this);
                break;
        }
    }



    /**
     *  Method is used to initialized check validation...
     */
    private void checkValidation() {
        if (!isValidEmail()) {
            return;
        }
        if (!isValidatePassword()) {
            return;
        }
        if (CommonUtils.isNetworkAvailable(Login.this)) {
            //doLoginService();
            mAppManager.createUserLoginSession("1", "", "", "", "");
            startActivity(new Intent(Login.this, HomeActivity.class));
            finish();
            Bungee.swipeLeft(Login.this);

        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), Login.this);
        }
    }


    private boolean isValidEmail() {
        if (!Validation.hasText(binding.emailEt, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(binding.emailEt, true, getString(R.string.error_email_invalid));
    }

    private boolean isValidatePassword() {
        if (!Validation.hasText(binding.pwdEt, getString(R.string.err_empty_password))) {
            return false;
        }
        return Validation.isValidPassword(binding.pwdEt, getString(R.string.error_pass_invalid));
    }

}
