package com.dev.potridersuser.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivityTermsConditionBinding;

import spencerstudios.com.bungeelib.Bungee;

public class TermsCondition extends AppCompatActivity {


    ActivityTermsConditionBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_condition);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }



        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(TermsCondition.this, Signup.class));
        finish();
        Bungee.swipeRight(TermsCondition.this);
    }
}
