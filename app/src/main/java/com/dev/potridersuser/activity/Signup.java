package com.dev.potridersuser.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dev.potridersuser.R;
import com.dev.potridersuser.databinding.ActivitySignupBinding;

import spencerstudios.com.bungeelib.Bungee;

public class Signup extends AppCompatActivity implements View.OnClickListener {


    ActivitySignupBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }



        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.signupRl.setOnClickListener(this);
        binding.loginLl.setOnClickListener(this);
        binding.termsTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signupRl:
                break;

            case R.id.loginLl:
                startActivity(new Intent(Signup.this, Login.class));
                finish();
                Bungee.swipeRight(Signup.this);
                break;

            case R.id.termsTv:
                startActivity(new Intent(Signup.this, TermsCondition.class));
                finish();
                Bungee.swipeLeft(Signup.this);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Signup.this, Login.class));
        finish();
        Bungee.swipeRight(Signup.this);
    }
}
