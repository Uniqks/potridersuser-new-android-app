package com.dev.potridersuser.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.dev.potridersuser.activity.Login;

import java.util.HashMap;

/**
 * Created by Seema on 26,February,2019
 */

public class PotRiderSessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    android.support.v4.app.Fragment fragment;
    int PRIVATE_MODE = 0;

    private static final String PREFER_NAME = "PotRiders";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String USERS_GROUP_ID = "users_group_id";
    public static final String IS_STAY_SIGNIN = "isstaysignedin";
    public static final String ID = "id";
    public static final String USER_NAME = "username";
    public static final String PWD = "pwd";
    public static final String EMAIL = "email";
    public static final String TOKEN = "token";


    public PotRiderSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public PotRiderSessionManager(android.support.v4.app.Fragment fragment) {
        this.fragment = fragment;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createUserLoginSession(String id, String userName, String email, String userGroupId, String token) {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(ID, id);
        editor.putString(USER_NAME, userName);
        editor.putString(EMAIL, email);
        editor.putString(USERS_GROUP_ID, userGroupId);
        editor.putString(TOKEN, token);
        editor.commit();
    }

    /*public boolean checkLogin() {
        if (!this.isUserLoggedIn()) {
            Intent i = new Intent(_context, Splash.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }*/

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        HashMap<String, Boolean> isSignIn = new HashMap<String, Boolean>();
        user.put(ID, pref.getString(ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(EMAIL, pref.getString(EMAIL, null));
        user.put(USERS_GROUP_ID, pref.getString(USERS_GROUP_ID, null));
        user.put(TOKEN, pref.getString(TOKEN, null));
        return user;
    }

    public HashMap<String, Boolean> getSignInDetails() {
        HashMap<String, Boolean> isSignIn = new HashMap<String, Boolean>();
        isSignIn.put(IS_STAY_SIGNIN, pref.getBoolean(IS_STAY_SIGNIN, false));
        return isSignIn;
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
}
