package com.dev.potridersuser.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Seema on 26,February,2019
 */

public class PotRiderApp extends Application {

    private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;
    private ExecutorService mExecutorService;
    private static PotRiderApp mInstance = null;
    private static final int CORE_POOL_SIZE = 5;
    private static Context context;

    private static String[] LOCATION_PERMISSIONS = new String[] {"android.permission.READ_PHONE_STATE", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};



    @Override
    public void onCreate() {

        super.onCreate();

        context = null;
        context = getApplicationContext();


        mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
        mInstance = this;

    }

    public static Context getGlobalContext() {
        return context;
    }

    public static Resources getAppResources() {
        return context.getResources();
    }

    public static String getAppString(int resourceId, Object... formatArgs) {
        return getAppResources().getString(resourceId, formatArgs);
    }

    public static String getAppString(int resourceId) {
        return getAppResources().getString(resourceId);
    }

    @Override
    public void onTerminate() {


        super.onTerminate();
    }

    public static interface OnLowMemoryListener {

        /**
         * Callback to be invoked when the system needs memory.
         */
        public void onLowMemoryReceived();
    }


    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(
                    listener));
        }
    }

    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            int i = 0;
            while (i < mLowMemoryListeners.size()) {
                final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
                if (l == null || l == listener) {
                    mLowMemoryListeners.remove(i);
                } else {
                    i++;
                }
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        int i = 0;
        while (i < mLowMemoryListeners.size()) {
            final OnLowMemoryListener listener = mLowMemoryListeners.get(i)
                    .get();
            if (listener == null) {
                mLowMemoryListeners.remove(i);
            } else {
                listener.onLowMemoryReceived();
                i++;
            }
        }
    }

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "GreenDroid thread #"
                    + mCount.getAndIncrement());
        }
    };

    public ExecutorService getExecutor() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE,
                    sThreadFactory);
        }
        return mExecutorService;
    }

    public static PotRiderApp getInstance() {
        return mInstance;
    }


    public void hideSoftKeyBoard(View view){
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean checkLocationPermission(Activity activity) {
        int readPhonePermission = ContextCompat.checkSelfPermission(activity, "android.permission.READ_PHONE_STATE");
        int accessCoarsePermission = ContextCompat.checkSelfPermission(activity, "android.permission.ACCESS_COARSE_LOCATION");
        int accessfinePermission = ContextCompat.checkSelfPermission(activity, "android.permission.ACCESS_FINE_LOCATION");

        if (readPhonePermission == 0 && accessCoarsePermission == 0 && accessfinePermission == 0) {
            return true;
        }
        ActivityCompat.requestPermissions(activity, LOCATION_PERMISSIONS, 1);
        return false;
    }

}

